import { takeLatest } from "redux-saga/effects";
import fetchVideos from "./videosSaga";

function* rootSaga() {
  yield takeLatest("SEARCH_TERM_UPDATE", fetchVideos);
}

export default rootSaga;
