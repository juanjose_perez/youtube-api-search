import { delay } from "redux-saga";
import { call, put } from "redux-saga/effects";
import axios from "axios";

const ROOT_URL = "https://www.googleapis.com/youtube/v3/search";
const API_KEY = "AIzaSyCqzYCLnLOAPcVhE0gGF6LnDXnuo_azEZo";

function* fetchVideos({ searchTerm }) {
  yield call(delay, 500);
  try {
    const params = {
      part: "snippet",
      key: API_KEY,
      q: searchTerm,
      type: "video"
    };
    const result = yield call(axios.get, ROOT_URL, { params });
    yield put({ type: "VIDEOS_FETCH_SUCCEEDED", videos: result.data.items });
    yield put({ type: "SELECT_VIDEO", selectedVideo: result.data.items[0] });
  } catch (e) {
    yield put({ type: "VIDEOS_FETCH_FAILED", message: e.message });
  }
}

export default fetchVideos;
