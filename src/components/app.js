import React from "react";
import PropTypes from "prop-types";
import SearchBar from "./searchBar";
import VideoList from "./videoList";
import VideoDetail from "./videoDetail";

const App = ({
  onVideoSelect,
  searchTerm,
  searchTermUpdate,
  selectedVideo,
  videos
}) => {
  return (
    <div>
      <SearchBar
        searchTerm={searchTerm}
        onSearchTermChange={searchTermUpdate}
      />
      <div className="row">
        <VideoDetail video={selectedVideo} />
        <VideoList onVideoSelect={onVideoSelect} videos={videos} />
      </div>
    </div>
  );
};

App.propTypes = {
  onVideoSelect: PropTypes.func,
  searchTerm: PropTypes.string,
  searchTermUpdate: PropTypes.func,
  selectedVideo: PropTypes.object,
  videos: PropTypes.array
};

export default App;
