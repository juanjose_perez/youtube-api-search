import React from "react";
import PropTypes from "prop-types";
import styles from "../styles/searchBar.css";

const SearchBar = props =>
  <div className={styles.searchBar}>
    <input
      value={props.searchTerm}
      onChange={event => props.onSearchTermChange(event.target.value)}
    />
  </div>;

SearchBar.propTypes = {
  searchTerm: PropTypes.string,
  onSearchTermChange: PropTypes.func
};

export default SearchBar;
