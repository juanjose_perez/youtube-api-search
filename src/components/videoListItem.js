import React from "react";
import PropTypes from "prop-types";
import styles from "../styles/videoListItem.css";

const VideoListItem = ({ video, onVideoClick }) => {
  const imageUrl = video.snippet.thumbnails.default.url;
  const title = video.snippet.title;
  return (
    <li
      className={`${styles.videoItem} list-group-item`}
      onClick={onVideoClick}
    >
      <div className="media">
        <div className="media-left">
          <img className="media-object" src={imageUrl} />
        </div>

        <div className="media-body">
          <div className="media-heading">{title}</div>
          <div />
        </div>
      </div>
    </li>
  );
};

VideoListItem.propTypes = {
  video: PropTypes.object,
  onVideoClick: PropTypes.func
};

export default VideoListItem;
