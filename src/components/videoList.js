import React from "react";
import PropTypes from "prop-types";
import VideoListItem from "./videoListItem";
import styles from "../styles/videoList.css";

const VideoList = ({ videos, onVideoSelect }) => {
  return (
    <ul className={`${styles.videoList} col-md-4`}>
      {videos.map(video =>
        <VideoListItem
          key={video.etag}
          video={video}
          onVideoClick={() => onVideoSelect(video)}
        />
      )}
    </ul>
  );
};

VideoList.propTypes = {
  videos: PropTypes.array,
  onVideoSelect: PropTypes.func
};

export default VideoList;
