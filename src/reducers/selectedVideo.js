function setSelectedVideo(state = {}, action) {
  return action.selectedVideo;
}

function selectedVideo(state = {}, action) {
  switch (action.type) {
    case "SELECT_VIDEO":
      return setSelectedVideo(selectedVideo, action);
    default:
      return state;
  }
}

export default selectedVideo;
