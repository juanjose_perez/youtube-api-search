function setVideos(state = [], action) {
  return action.videos;
}

function videos(state = [], action) {
  switch (action.type) {
    case "VIDEOS_FETCH_SUCCEEDED":
      return setVideos(state, action);
    default:
      return state;
  }
}

export default videos;
