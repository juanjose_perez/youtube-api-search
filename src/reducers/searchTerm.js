function setSearchTerm(state = "", action) {
  return action.searchTerm;
}

function searchTerm(state = "", action) {
  switch (action.type) {
    case "SEARCH_TERM_UPDATE":
      return setSearchTerm(state, action);
    default:
      return state;
  }
}

export default searchTerm;
