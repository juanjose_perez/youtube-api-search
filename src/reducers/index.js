import { combineReducers } from "redux";
import selectedVideo from "./selectedVideo";
import searchTerm from "./searchTerm";
import videos from "./videos";

const rootReducer = combineReducers({
  videos,
  selectedVideo,
  searchTerm
});

export default rootReducer;
