import React, { Component } from "react";
import PropTypes from "prop-types";
import App from "../components/app";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../actions";

class AppContainer extends Component {
  constructor(props) {
    super(props);
    this.props.searchTermUpdate("ejemplo");
  }

  render() {
    return (
      <App
        videos={this.props.videos}
        selectedVideo={this.props.selectedVideo}
        searchTerm={this.props.searchTerm}
        searchTermUpdate={this.props.searchTermUpdate}
        onVideoSelect={selectedVideo => this.props.selectVideo(selectedVideo)}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    videos: state.videos,
    selectedVideo: state.selectedVideo,
    searchTerm: state.searchTerm
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

AppContainer.propTypes = {
  videos: PropTypes.array,
  searchTerm: PropTypes.string,
  searchTermUpdate: PropTypes.func,
  selectedVideo: PropTypes.object,
  selectVideo: PropTypes.func
};

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);
