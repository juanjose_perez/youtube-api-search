/* global module */

import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { AppContainer as AppHotContainer } from "react-hot-loader";

import store from "./store";
import AppContainer from "./containers/app";

const render = Component => {
  ReactDOM.render(
    <AppHotContainer>
      <Provider store={store}>
        <Component />
      </Provider>
    </AppHotContainer>,
    document.querySelector(".container")
  );
};

render(AppContainer);
if (module && module.hot) {
  module.hot.accept("./containers/app", () => render(AppContainer));
}
