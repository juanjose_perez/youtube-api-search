export function selectVideo(selectedVideo) {
  return {
    type: "SELECT_VIDEO",
    selectedVideo
  };
}

export function searchTermUpdate(searchTerm) {
  return {
    type: "SEARCH_TERM_UPDATE",
    searchTerm
  };
}
