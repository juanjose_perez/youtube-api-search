/*global process module require __dirname*/
var path = require("path");
var webpack = require("webpack");
var HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  devtool: "eval",
  devServer: {
    hot: true,
    compress: true,
    contentBase: path.resolve(__dirname),
    publicPath: "/",
    historyApiFallback: true
  },
  context: path.resolve(__dirname, "src"),
  entry: [
    "babel-polyfill",
    "react-hot-loader/patch",
    "webpack-hot-middleware/client?reload=true",
    "./index.js"
  ],
  output: {
    path: path.resolve(__dirname),
    publicPath: "/",
    filename: "bundle.js"
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.ProgressPlugin(),
    new HtmlWebpackPlugin({
      template: "index.pug"
    })
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        include: [path.resolve(__dirname, "src")],
        exclude: [path.resolve(__dirname, "node_modules")],
        loader: "babel-loader",
        query: {
          presets: ["react", ["es2015", { modules: false }], "stage-2"]
        }
      },
      {
        test: /.css$/,
        include: [path.resolve(__dirname, "src")],
        exclude: [path.resolve(__dirname, "node_modules")],
        use: [
          {
            loader: "style-loader"
          },
          {
            loader: "css-loader",
            options: {
              importLoaders: 0,
              modules: true,
              localIdentName: "[name]__[local]___[hash:base64:5]",
              camelCase: true
            }
          },
          {
            loader: "postcss-loader"
          }
        ]
      },
      {
        test: /\.pug$/,
        include: [path.resolve(__dirname, "src")],
        exclude: [path.resolve(__dirname, "node_modules")],
        use: [
          {
            loader: "html-loader"
          },
          {
            loader: "pug-html-loader",
            options: {
              data: {
                pageTitle: "Development build",
                production: process.env.ENV === "production"
              }
            }
          }
        ]
      }
    ]
  }
};
